import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        JFrame window = new JFrame();
        GamePlay gamePlay = new GamePlay();
        window.setBounds(20,20,705,600);
        window.setTitle("Breakout balls");
        window.setResizable(false);
        window.setVisible(true);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.add(gamePlay);
    }
}
