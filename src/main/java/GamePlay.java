import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GamePlay extends JPanel implements KeyListener, ActionListener {

    private boolean play = false;
    private int score = 0;

    private int totalBricks = 21;
    private BrickGenerator brickGenerator;

    private Timer timer;

    private int paddlePosX = 310;

    private int ballPosX = 120;
    private int ballPosY = 350;
    private int ballPosXDirection = -1;
    private int ballPosYDirection = -2;

    private int row = 3;
    private int col = 7;

    public GamePlay() {
        brickGenerator = new BrickGenerator(row,col);
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(true);
        timer = new Timer(8, this);
        timer.start();
    }

    @Override
    public void paint(Graphics graphics) {
        //background color
        graphics.setColor(Color.black);
        graphics.fillRect(0, 0, 705, 600);

        //borders color
        graphics.setColor(Color.yellow);
        graphics.fillRect(0, 0, 3, 600);
        graphics.fillRect(0, 0, 705, 3);
        graphics.fillRect(696, 0, 3, 600);

        //score
        graphics.setColor(Color.white);
        graphics.setFont(new Font("serif", Font.BOLD, 25));
        graphics.drawString(""+score, 590, 30);

        //brickGenerator
        brickGenerator.draw((Graphics2D)graphics);

        //paddle
        graphics.setColor(Color.green);
        graphics.fillRect(paddlePosX, 550, 100, 10);

        //ball
        graphics.setColor(Color.red);
        graphics.fillOval(ballPosX, ballPosY, 20, 20);

        if(ballPosY>570) {
            endGame(graphics);
        } else if(totalBricks ==0) {
            wonLevel(graphics);
        }
        graphics.dispose();
    }

    private void endGame(Graphics graphics) {
        play = false;
        graphics.setColor(Color.red);
        graphics.setFont(new Font("serif", Font.BOLD, 25));
        graphics.drawString("Game Over, Scores: " + score, 240, 300);
        graphics.drawString("Press enter to play again", 230, 330 );
    }

    private void wonLevel(Graphics graphics) {
        play = false;
        graphics.setColor(Color.red);
        graphics.setFont(new Font("serif", Font.BOLD, 25));
        graphics.drawString("You won", 300, 300 );
        graphics.drawString("Press space to play next level", 200, 330 );
    }


    public void actionPerformed(ActionEvent e) {
        timer.start();
        if (play) {
            Rectangle ball = new Rectangle(ballPosX, ballPosY, 20,20);
            Rectangle paddle = new Rectangle(paddlePosX, 550, 100,10);

            for(int i = 0; i< brickGenerator.map.length; i++) {
                for(int j = 0; j< brickGenerator.map[0].length; j++) {
                    brickInitialize(ball, i, j);
                }
            }

            if(ballPosX<5 || ballPosX>675) {
                ballPosXDirection = -ballPosXDirection;
            }
            if(ballPosY<5 || ball.intersects(paddle)) {
                ballPosYDirection = -ballPosYDirection;
            }

            ballPosX += ballPosXDirection;
            ballPosY += ballPosYDirection;
        }
        repaint();
    }

    private void brickInitialize(Rectangle ball, int i, int j) {
        if(brickGenerator.map[i][j] >0) {
            int brickX = j* brickGenerator.brickWidth+80;
            int brickY = i* brickGenerator.brickHeight+50;
            int brickWidth = brickGenerator.brickWidth;
            int brickHeight = brickGenerator.brickHeight;

            Rectangle brick = new Rectangle(brickX, brickY, brickWidth, brickHeight);
            if(ball.intersects(brick)) {
                breakBrick(i, j, brickWidth, brick);
            }
        }
    }

    private void breakBrick(int i, int j, int brickWidth, Rectangle brick) {
        brickGenerator.setBrickValue(0, i, j);
        totalBricks--;
        score+=5;
        if(ballPosX +19 <=brick.x|| ballPosX + 1 >= brick.x+brickWidth) {
            ballPosXDirection = -ballPosXDirection;
        } else {
            ballPosYDirection =- ballPosYDirection;
        }
    }

    public void keyTyped(KeyEvent e) {}
    public void keyReleased(KeyEvent e){}

    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
            if(paddlePosX <590) {
                moveRight();
            }
        } else if(e.getKeyCode() == KeyEvent.VK_LEFT) {
            if(paddlePosX >10) {
                moveLeft();
            }
        } else if(e.getKeyCode() == KeyEvent.VK_ENTER) {
            if(!play) {
                newGame();
            }
        } else if(e.getKeyCode() == KeyEvent.VK_SPACE) {
            if(!play) {
                nextLevel();
            }
        }
    }

    private void newGame() {
        ballPosX = 120;
        ballPosY = 350;
        ballPosXDirection = -1;
        ballPosYDirection =-2;
        paddlePosX = 310;
        score = 0;
        row = 3;
        col = 7;
        totalBricks = row*col;
        brickGenerator = new BrickGenerator(row, col);
        play = true;
    }

    private void nextLevel() {
        ballPosX = 120;
        ballPosY = 350;
        ballPosXDirection = -1;
        ballPosYDirection =-2;
        paddlePosX = 310;
        row = row*2;
        col = col*2;
        totalBricks = row*col;
        brickGenerator = new BrickGenerator(row, col);
        play = true;
    }

    private void moveRight() {
        play = true;
        paddlePosX+=20;
    }

    private void moveLeft() {
        play = true;
        paddlePosX-=20;
    }
}
